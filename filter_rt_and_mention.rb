#!/home/matsuda/.rbenv/shims/ruby

require 'oj'

while line = gets()
  o = Oj.load(line.chomp)
  if o['text'].to_s.start_with?("RT")
    next
  end
  if o['text'].to_s.include?("@")
    next
  end
  puts line
end
