#!/home/matsuda/.rbenv/shims/ruby

require 'oj'

def valid_time_zone?(tz)
  return true if [nil, "Tokyo", "Osaka", "Asia/Tokyo", "Sapporo", "Irkutsk", "JST"].include?(tz)
  return false
end

while line = gets()
  o = Oj.load(line)
  tz = o['user']['time_zone']
  next unless valid_time_zone?(tz)
  puts line
end
