#!/home/matsuda/2.7/bin/python
# -*- coding: utf-8 -*-
# 日本語ツイートを対象
# ブラックリストでは多種多様なclient名を用いるbot達に対応できないことから，
# ホワイトリストで対応．

# 入れるべきか悩むもの
# <a href="http://www.hootsuite.com" rel="nofollow">Hootsuite</a>
# <a href="http://www.tweetdeck.com" rel="nofollow">TweetDeck</a>
#   普通のクライアントかと思ったが，BOT機能がある？
#   とりあえず除外．
# <a href="https://dev.twitter.com/docs/tfw" rel="nofollow">Twitter for Websites</a>
#   ウェブサイト埋め込み型のクライアントであるため，扱いが難しい．
#   とりあえず除外．
#

# 入れてはいるが要検討
# web

import json
import sys

sources = ur"""
<a href="http://twitter.com/download/iphone" rel="nofollow">Twitter for iPhone</a>
<a href="http://instagram.com" rel="nofollow">Instagram</a>
<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>
<a href="http://janetter.net/" rel="nofollow">Janetter</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる for iPhone</a>
<a href="https://twitter.com/#!/ABS104a" rel="nofollow">Biyon≡(　ε:)</a>
<a href="http://twitter.com/download/android" rel="nofollow">Twitter for Android</a>
<a href="http://tapbots.com/tweetbot" rel="nofollow">Tweetbot for iΟS</a>
<a href="http://twitter.com/#!/download/ipad" rel="nofollow">Twitter for iPad</a>
<a href="http://theworld09.com/" rel="nofollow">TheWorld for iOS　</a>
<a href="http://twtr.jp" rel="nofollow">Keitai Web</a>
<a href="http://www.echofon.com/" rel="nofollow">Echofon</a>
<a href="http://jigtwi.jp/?p=1" rel="nofollow">jigtwi</a>
<a href="https://about.twitter.com/products/tweetdeck" rel="nofollow">TweetDeck</a>
<a href="https://twitter.com/ABS104a" rel="nofollow">Biyon≡(　ε:) Pro</a>
<a href="https://itunes.apple.com/us/app/hel1um/id574259931?mt=8&uo=4" rel="nofollow">Hel1um on iOS</a>
<a href="https://sites.google.com/site/tweentwitterclient/" rel="nofollow">Tween</a>
<a href="http://twitter.suruyatu.com/" rel="nofollow">ツイッターするやつ</a>
<a href="https://sites.google.com/site/wakamesoba98/sobacha" rel="nofollow">SobaCha</a>
<a href="http://shootingstar067.com/" rel="nofollow">ShootingStarPro</a>
<a href="http://twicca.r246.jp/" rel="nofollow">twicca</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる for Android</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる　</a>
<a href="https://mobile.twitter.com" rel="nofollow">Mobile Web (M5)</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる Pro for iPhone</a>
<a href="http://www.apple.com" rel="nofollow">iOS</a>
<a href="http://adobe.com" rel="nofollow">Adobe® Social</a>
<a href="http://sites.google.com/site/yorufukurou/" rel="nofollow">YoruFukurou</a>
<a href="https://twitter.com/download/android" rel="nofollow">Twitter for Android Tablets</a>
<a href="http://www.movatwi.jp" rel="nofollow">モバツイsmart / www.movatwi.jp</a>
<a href="http://twitter.softama.com/" rel="nofollow">ツイタマ</a>
<a href="http://tweetlogix.com" rel="nofollow">Tweetlogix</a>
<a href="http://www.justsystems.com/jp/products/tweetatok/" rel="nofollow">Tweet ATOK</a>
<a href="http://janetter.net/" rel="nofollow">Janetter for Mac</a>
<a href="http://www.movatwi.jp" rel="nofollow">モバツイ / www.movatwi.jp</a>
<a href="http://theworld09.com/" rel="nofollow">TheWorld⠀</a>
<a href="http://www.ustream.tv" rel="nofollow">Ustream.TV</a>
<a href="http://klinkerapps.com" rel="nofollow">Talon (Classic)</a>
<a href="http://twitter.softama.com/" rel="nofollow">ツイタマ for Android</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる Pro for Android　</a>
<a href="http://yubitter.com/" rel="nofollow">yubitter</a>
<a href="http://sinproject.net/tweecha/" rel="nofollow">tweecha for android</a>
<a href="http://www.facebook.com/twitter" rel="nofollow">Facebook</a>
<a href="http://www.movatwi.jp" rel="nofollow">モバツイ / www.movatwi.jp .</a>
<a href="http://janetter.net/" rel="nofollow">Janetter for iPhone</a>
<a href="http://www.tweetcaster.com" rel="nofollow">TweetCaster for Android</a>
<a href="http://itunes.apple.com/us/app/twitter/id409789998?mt=12" rel="nofollow">Twitter for Mac</a>
<a href="http://tweetli.st/" rel="nofollow">TweetList!</a>
<a href="https://twitter.com/TheWorld_JP" rel="nofollow">TheWorld for iOS</a>
<a href="http://vine.co" rel="nofollow">Vine - Make a Scene</a>
<a href="http://www.tweetiumapp.com" rel="nofollow">Tweetium for Windows</a>
<a href="http://www.twitter.com" rel="nofollow">Twitter for Windows</a>
<a href="http://cdn.movatwi.jp/contents/touch/index.html" rel="nofollow">モバツイtouch</a>
<a href="http://jigtwi.jp/?p=1001" rel="nofollow">jigtwi for Android</a>
<a href="http://apps.studiohitori.com/twitrocker" rel="nofollow">TwitRocker2 for iPhone</a>
<a href="http://yoshika23218.com/" rel="nofollow">twitcle plus</a>
<a href="https://chrome.google.com/extensions/detail/encaiiljifbdbjlphpgpiimidegddhic" rel="nofollow">Silver Bird</a>
<a href="http://twitter.com/tweetbutton" rel="nofollow">Tweet Button</a>
<a href="http://tapbots.com/tweetbot" rel="nofollow">Tweetbot for iOS</a>
<a href="https://mobile.twitter.com/lefty_bass" rel="nofollow">TheWorld for Lefty</a>
<a href="http://bit.ly/UDldit" rel="nofollow">Saezuri</a>
<a href="https://sourceforge.jp/projects/opentween/" rel="nofollow">OpenTween</a>
<a href="http://janetter.net/" rel="nofollow">Janetter Pro for iPhone</a>
<a href="http://janetter.net/" rel="nofollow">Janetter Pro for Android</a>
<a href="http://yoshika23218.com/twitcle/" rel="nofollow">twitcle</a>
<a href="http://tweetli.st/" rel="nofollow">TweetList Pro</a>
<a href="http://nhk.jp/netradio" rel="nofollow">NHK Net Radio for iOS</a>
<a href="http://twitnow.groovymedia.biz/" rel="nofollow">twitなう</a>
<a href="http://www.crouto.com/teewee" rel="nofollow">Teewee</a>
<a href="http://f.tabtter.jp" rel="nofollow">Tabtter Free</a>
<a href="http://stone.com/Twittelator" rel="nofollow">Twittelator</a>
<a href="http://janetter.net/" rel="nofollow">Janetter for Android</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる for iPad</a>
<a href="http://blackberry.com/twitter" rel="nofollow">Twitter for BlackBerry®</a>
<a href="http://tabtter.jp" rel="nofollow">Tabtter</a
<a href="http://jigtwi.jp/?p=2001" rel="nofollow">jigtwi for iPhone</a>
<a href="http://tapbots.com/software/tweetbot/mac" rel="nofollow">Tweetbot for Mac</a>
<a href="http://bskt.yubais.net/" rel="nofollow">BossKitter.</a>
<a href="http://mixi.jp/promotion.pl?id=voice_twitter" rel="nofollow"> mixi ボイス </a>
<a href="http://covelline.com/feather/" rel="nofollow">feather for iOS</a>
<a href="http://labo.willcomnews.com/?cid=56904" rel="nofollow">WNTwit</a>
<a href="https://mobile.twitter.com" rel="nofollow">Mobile Web (M2)</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる/twipple</a>
<a href="http://www.nibirutech.com" rel="nofollow">TwitBird</a>
<a href="http://seesmic.com/" rel="nofollow">Seesmic<z
<a href="http://www.apple.com/" rel="nofollow">OS X</a>
<a href="http://www.soicha.com" rel="nofollow">SOICHA</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる   for iPhone</a>
<a href="http://twipple.jp/" rel="nofollow">ついっぷる/twipple</a>
<a href="http://shootingstar067.com/" rel="nofollow">ShootingStar</a>
<a href="http://www.apple.com" rel="nofollow">Photos on iOS</a>
<a href="http://www.crowy.net/" rel="nofollow">Crowy</a>
<a href="http://stone.com/neue" rel="nofollow">Twittelator Neue</a>
web
""".split("\n")

def ishuman(tweet):
    return tweet['source'] in sources

if __name__ == '__main__':
    nf = True if len(sys.argv) > 1 and sys.argv[1] == "n" else False
    for line in sys.stdin:
        line = line.rstrip('\n')
        J = json.loads(line)
        if (ishuman(J) and not nf) or (not ishuman(J) and nf):
            print line

