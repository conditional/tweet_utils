#!/home/matsuda/.rbenv/shims/ruby

require 'oj'

while line = gets()
  o = Oj.load(line.chomp)
  puts line unless o['text'].to_s.include?("http")
end
